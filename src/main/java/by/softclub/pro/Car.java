package by.softclub.pro;

import java.math.BigDecimal;

public enum Car {

    BMW("bmw", new BigDecimal(15)),
    OPEL("opel", new BigDecimal(12)),
    FORD("ford", new BigDecimal(13));

    private String name;
    private BigDecimal price;

    Car(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
