package by.softclub.pro;

import java.math.BigDecimal;
import java.util.Map;

public class CurrencyExchange {

    private Map<String, Converter> converters;

    public CurrencyExchange(Map<String, Converter> converters) {
        this.converters = converters;
    }

    public void doConvert(String currency, BigDecimal price) {
        System.out.println("Price in BYN: " + price);
        System.out.println("Price in " + currency.toUpperCase() + ": " +
                converters.get(currency).convertInCurr(price));
    }
}
