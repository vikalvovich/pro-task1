package by.softclub.pro;

public enum Currency {

    EURO("euro"),
    RUB("rub"),
    USD("usd");

    private String name;

    Currency(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
