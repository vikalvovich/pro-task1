package by.softclub.pro;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class TestSpring {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        if (args.length > 1) {
            if (args.length > 2) {
                try {
                    PrintStream o = new PrintStream(new File(args[2]));
                    System.setOut(o);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            Currency currency = Currency.valueOf(args[0].toUpperCase());
            Car car = Car.valueOf(args[1].toUpperCase());
            System.out.println(currency.getName());
            System.out.println(car.getPrice());

            CurrencyExchange currencyExchange = context.getBean("currencyExchange", CurrencyExchange.class);
            currencyExchange.doConvert(currency.getName(), car.getPrice());
        }
    }
}
