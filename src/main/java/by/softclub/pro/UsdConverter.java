package by.softclub.pro;

import java.math.BigDecimal;

public class UsdConverter implements Converter {
    private double rate;

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public BigDecimal convertInCurr(BigDecimal price) {
        return price.multiply(new BigDecimal(rate));
    }
}
