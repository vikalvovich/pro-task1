package by.softclub.pro;

import java.math.BigDecimal;

public interface Converter {
     BigDecimal convertInCurr(BigDecimal price);
}
